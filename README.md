HTML5 & CSS3 Template
=============

'ithinkinco.de' is a design I developed for my website [ithinkinco.de](http://ithinkinco.de). Though I have yet to convert it to an octopress theme, which is what my site is built on. I decided that since there were so few good HTML5 & CSS3 templates to release this. It's also good to read the code if you are just learning about HTML5 & CSS3.

Contributions
-------------

If you'd like to add to this design just fork it. I'm open to constructive criticism and additions
to my design.

Enjoy
-----